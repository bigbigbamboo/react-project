
// 导入模块
import React, { Component } from 'react'
// 导入样式
import { LoginDiv } from './style'
// 导入UI组件
import { 
    Card, 
    Form, Icon, Input, Button, Checkbox,
} from 'antd';
 class Login extends Component {
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
          }
        });
      };
    
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <LoginDiv>
                    <Card title="后台管理系统">
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: '请输入用户名!' }],
                        })(
                            <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="请输入用户名"
                            />,
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: '请输入密码!' }],
                        })(
                            <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="请输入密码"
                            />,
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(<Checkbox>记住我</Checkbox>)}
                        {/* <a className="login-form-forgot" href="http://www.baidu.com">
                            Forgot password
                        </a> */}
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            登录
                        </Button>
                        {/* Or <a href="http://www.baidu.com">register now!</a> */}
                        </Form.Item>
                    </Form>
                    </Card>
            </LoginDiv>            
        )
    }
}

// export default Login
const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);
// ReactDOM.render(<WrappedNormalLoginForm />, mountNode);
export default WrappedNormalLoginForm
// 导入库
import React,{Component} from 'react';
import {
    HashRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';

// 导入组件
import Login from '../pages/login/index';
import Admin from '../pages/admin/index';
import Err404 from '../components/err/404';
import Err500 from '../components/err/500';

class ReactRouter extends Component
{
    render()
    {
        return (
            <Router>
                <Switch>
                    <Route path="/login" component={Login}></Route>
                    <Route path="/admin">
                        <Switch>
                        {/* 
                        备注：后期这里有很多很多路由
                        举例：订单路由、商品路由等
                        */}
                        <Route path="/admin" exact component={Admin}></Route>
                        </Switch>
                    </Route>
                    <Route path="/404" component={Err404}></Route>
                    <Route path="/500" component={Err500}></Route>
                    <Redirect to="/404" /> 
                </Switch> 
            </Router>
        )
    } 
}

export default ReactRouter;
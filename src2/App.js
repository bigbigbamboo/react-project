import React from 'react';
import logo from './logo.svg';
import './App.css';

import { 
  Button,
  Pagination
 } from 'antd';

 function itemRender(current, type, originalElement) {
  if (type === 'prev') {
    return <a href="http://baidu.com">Previous</a>;
  }
  if (type === 'next') {
    return <a href="http://baidu.com">Next</a>;
  }
  return originalElement;
}

function App() {
  return (
    <div className="App">
       <div>
        <Button type="primary">Primary</Button>
        <Button>Default</Button>
        <Button type="dashed">Dashed</Button>
        <Button type="danger">Danger</Button>
        <Button type="link">Link</Button>
      </div>,
      <Pagination total={500} itemRender={itemRender} />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
